create table users2(
	id int(10) not null auto_increment,	
	name varchar(255) not null,
	email varchar(255) not null,
	tel varchar(255) not null,
	message varchar(255) not null,
	ip varchar(255) ,
	ua varchar(255),
	created_at timestamp not null default 0,
	updated_at timestamp not null default current_timestamp on update current_timestamp,
	flg int(1) not null default 1,
	primary key(id)
);

alter table users2 change flg flg int(1) default 1;

insert into users2 values(
	null,'hanaikazuki',"kazuki.hanai@hanai.jp","000000000000","こんにちは","127.0.0.1","mac",null,null,null
);

insert into users2 values(
	null,"hanaichiharu","chiharu.hanai@hanai.jp","00000000000","母です","127.0.0.1","windows10",null,null,null
);


create table prefecture(
	id int(10) not null auto_increment,
	prefecture_name varchar(255) not null,
	primary key(id)
);

insert into prefecture 	values(null,"長野県"),(null,"新潟県"),(null,"富山県"),
	(null,"石川県"),(null,"福井県"),(null,"岐阜県"),(null,"静岡県"),
	(null,"愛知県"),(null,"三重県"),(null,"滋賀県"),(null,"京都府"),
	(null,"大阪府"),(null,"兵庫県"),(null,"奈良県"),(null,"和歌山県"),
	(null,"鳥取県"),(null,"島根県"),(null,"岡山県"),(null,"広島県"),
	(null,"山口県"),(null,"徳島県"),(null,"香川県"),(null,"愛媛県"),
	(null,"高知県"),(null,"福岡県"),(null,"佐賀県"),(null,"長崎県"),
	(null,"熊本県"),(null,"大分県"),(null,"宮崎県"),(null,"鹿児島県"),(null,"沖縄県");


select users2.id, users2.name, users2.email, prefecture.prefecture_name from users2 inner join prefecture on users2.prefecture = prefecture.id;

insert into users2 values(
	null,'野比のび太',"nobi@nobita.jp","123424456248","のび太です","127.0.0.1","mac",null,null,null
);


create table users3(
	id int(10) not null auto_increment,
	name varchar(255) not null,
	email varchar(255) not null,
	tel varchar(255),
	message varchar(255),
	ip varchar(255) not null,
	ua varchar(255) not null,
	created_at timestamp not null default 0,
	updated_at timestamp not null default current_timestamp on update current_timestamp,
	flg int(1) not null default 1,
	primary key(id)
);