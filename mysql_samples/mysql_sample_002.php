<?php

/**
* 文字コード設定
*/
	/*mb_language("uni");
	mb_internal_encoding("utf-8");
	mb_http_input("auto");
	mb_http_output("utf-8");

/**
* MySQL接続設定
*/
	define('DB_NAME',		'user');
	define('DB_USER',		'hanai');
	define('DB_PASSWORD',	'Kazuki56');
	define('DB_HOST',		'localhost');
	define('DB_PORT',		'3306');
	define('DB_CHARSET',	'utf8');
	define('DB_TYPE',     'mysql');


/**
 * MySQL 接続テスト
 */
	$db_dsn = DB_TYPE.':host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME.';charset='.DB_CHARSET;

	try
	{
	  $dbh = new PDO($db_dsn, DB_USER, DB_PASSWORD);
	  if($dbh == null)
	  {
	    print('接続に失敗しました。<br>');
	  }
	  else
	  {
	    print('接続に成功しました。<br>');
	  }
	}
	catch(PDOException $e)
	{
	    print('Error:'.$e->getMessage());
	    die();
	}


/**
 * MySQL クエリ実行テスト
 */
	$sql = "SELECT * FROM users2";
	$result = $dbh->query($sql);
	if(!$result)
	{
	    echo "クエリ実行できませんでした";
	    print_r($dbh->errorInfo());
	    die();
	}
	else
	{
	   echo 'クエリ実行に成功しました<br>';
	}


/**
 * 取得結果出力
 */
	$result = $result->fetchAll();
	var_dump($result);


?>

<!DOCTYPE html>
<head>
	<title>MySQLフォーム</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
    <link type="text/css" rel="stylesheet" href="./css/style.css">
</head>
<body>
<div id="all">
	<form action="mysql_sample_002_add_user.php" method="post" id="register">
		<dl>
			<dt>
				<label for="name">お名前</label> <em>必須</em>
			</dt>
			<dd>
				<input type="text" name="name" id="name" class="size-mini" placeholder="山田 太郎" value="">
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="mail">メールアドレス</label> <em>必須</em>
			</dt>
			<dd>
				<input type="text" name="mail" id="mail" class="size-half" placeholder="yamada@example.com" value="">
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="prefecture">都道府県</label>
			</dt>
			<dd>
				<select id="prefecture" name="prefecture">
                        <option value="0">（未選択）</option>
						<option value="1">北海道</option>
						<option value="2">青森県</option>
						<option value="3">秋田県</option>
						<option value="4">岩手県</option>
						<option value="5">山形県</option>
						<option value="6">宮城県</option>
						<option value="7">福島県</option>
						<option value="8">茨城県</option>
						<option value="9">栃木県</option>
						<option value="10">群馬県</option>
						<option value="11">埼玉県</option>
						<option value="12">神奈川県</option>
						<option value="13">千葉県</option>
						<option value="14">東京都</option>
						<option value="15">山梨県</option>
						<option value="16">長野県</option>
						<option value="17">新潟県</option>
						<option value="18">富山県</option>
						<option value="19">石川県</option>
						<option value="20">福井県</option>
						<option value="21">岐阜県</option>
						<option value="22">静岡県</option>
						<option value="23">愛知県</option>
						<option value="24">三重県</option>
						<option value="25">滋賀県</option>
						<option value="26">京都府</option>
						<option value="27">大阪府</option>
						<option value="28">兵庫県</option>
						<option value="29">奈良県</option>
						<option value="30">和歌山県</option>
						<option value="31">鳥取県</option>
						<option value="32">島根県</option>
						<option value="33">岡山県</option>
						<option value="34">広島県</option>
						<option value="35">山口県</option>
						<option value="36">徳島県</option>
						<option value="37">香川県</option>
						<option value="38">愛媛県</option>
						<option value="39">高知県</option>
						<option value="40">福岡県</option>
						<option value="41">佐賀県</option>
						<option value="42">長崎県</option>
						<option value="43">熊本県</option>
						<option value="44">大分県</option>
						<option value="45">宮崎県</option>
						<option value="46">鹿児島県</option>
						<option value="47">沖縄県</option>
                    </select><br>
				</select>
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="tel">電話番号</label>
			</dt>
			<dd>
				<input type="text" name="tel" id="tel" class="size-half" placeholder="053-464-7708" value="">
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="message">メッセージ</label>
			</dt>
			<dd>
				<textarea name="message" id="message" class="size-full" rows="5" placeholder="ここにメッセージを記載してください" ></textarea>
			</dd>
		</dl>

		<div id="buttons">
		<input type="submit" value="リセット" class="button back">
		&emsp;&emsp;&emsp;&emsp;&emsp;
		<input type="submit" value="確認画面へ" class="button">
		</div>
	</form>
</div>
</body>
</html>