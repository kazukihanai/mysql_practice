<?php

/**
* 文字コード設定
*/
	mb_language("uni");
	mb_internal_encoding("utf-8");
	mb_http_input("auto");
	mb_http_output("utf-8");


/**
* 外部ファイル読み込み
*/
    include 'check_string.php';


/**
* セキュリティ設定
*/
	header('X-FRAME-OPTIONS:DENY');


	$post_value = array();

	foreach ($_POST as $key => $value)
	{
		$post_value = $post_value + array($key => htmlspecialchars($value));

        $post_value[$key] = mb_convert_kana($post_value[$key],"asH");

        $post_value[$key] = str_replace("\0", "", $post_value[$key]);
	}

	$check_string =  new check_string($post_value);

	$_SESSION['error_message'] = $check_string->get_error_message();

	if($check_string->is_set_error_flag())
    {
        header('Location: mysql_sample_002.php');
        exit;
    }

/**
* MySQL接続設定
*/
	define('DB_NAME',		'user');
	define('DB_USER',		'hanai');
	define('DB_PASSWORD',	'Kazuki56');
	define('DB_HOST',		'localhost');
	define('DB_PORT',		'3306');
	define('DB_CHARSET',	'utf8');
	define('DB_TYPE',     'mysql');


/**
 * MySQL 接続テスト
 */
	$db_dsn = DB_TYPE.':host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME.';charset='.DB_CHARSET;

	try
	{
	  $dbh = new PDO($db_dsn, DB_USER, DB_PASSWORD);
	  if($dbh == null)
	  {
	    print('接続に失敗しました。<br>');
	  }
	  else
	  {
	    print('接続に成功しました。<br>');
	  }
	}
	catch(PDOException $e)
	{
	    print('Error:'.$e->getMessage());
	    die();
	}


/**
* ユーザ追加
*/
	$name = $post_value['name'];
	$mail = $post_value['mail'];
	$prefecture = $post_value['prefecture'];
	$tel = $post_value['tel'];
	$message = $post_value['message'];
	$ip = "192.168.33.10";
	$ua = "unknown";

	$sql_add = 'insert ignore into users2 values(
					null, 
					"'.$name.'",
					"'.$mail.'",
					"'.$prefecture.'",
					"'.$tel.'",
					"'.$message.'",
					"'.$ip.'",
					"'.$ua.'",
					null,
					null,
					1
				)';


	$result = $dbh->query($sql_add);

	if(!$result)
	{
	    print "クエリ実行できませんでした";
	    print_r($dbh->errorInfo());
	    die();
	}
	else
	{
	   print 'クエリ実行に成功しました<br>';
	}


